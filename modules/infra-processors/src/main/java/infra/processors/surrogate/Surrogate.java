package infra.processors.surrogate;

/**
 * A common superclass to all surrogate types. It provides the handle. This class is used for simple unpacking of
 * surrogates in mixins.
 *
 * @param <T>
 */
public class Surrogate<T> {
    public T handle;
}
