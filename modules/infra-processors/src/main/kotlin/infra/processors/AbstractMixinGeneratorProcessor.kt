package infra.processors

import com.squareup.javapoet.ClassName
import com.squareup.javapoet.CodeBlock
import com.squareup.javapoet.TypeName
import com.vdurmont.semver4j.Semver
import infra.processors.injection.TargetType
import infra.processors.surrogate.GenerateSurrogatesProcessor
import infra.processors.surrogate.PlatformSurrogate
import infra.processors.surrogate.Surrogated
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import javax.lang.model.type.*
import javax.tools.Diagnostic
import kotlin.reflect.KClass

abstract class AbstractMixinGeneratorProcessor(private val annotation: KClass<out Annotation>) : AbstractProcessor() {

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latestSupported()
    }

    override fun process(annotations: MutableSet<out TypeElement>, roundEnv: RoundEnvironment): Boolean {
        val mcVersion = processingEnv.options["mc_version"]
        if (mcVersion == null) {
            processingEnv.messager
                .printMessage(
                    Diagnostic.Kind.ERROR,
                    "${annotation.simpleName} cannot be processed without minecraft version defined"
                )
        }
        val semanticVersion = Semver(mcVersion, Semver.SemverType.NPM)

        // process all elements annotated with GenerateInjections (should only be one dummy object)
        roundEnv.getElementsAnnotatedWith(annotation.java)
            .forEach { element -> generateMixins(element, semanticVersion) }

        return true
    }

    /**
     * Generate the mixins that result from the annotated dummy element
     *
     * @param dummy the annotated dummy element
     * @param semanticVersion the semantic minecraft platform version to generate for
     */
    abstract fun generateMixins(dummy: Element, semanticVersion: Semver)

    /**
     *  Try to map the given type mirror to a platform type. If that fails, return the input type mirror
     */
    protected fun tryMapToRuntimeType(typeMirror: TypeMirror, semanticVersion: Semver): TypeMirror {
        return try {
            mapToRuntimeType(typeMirror, semanticVersion)
        } catch (e: java.lang.IllegalArgumentException) {
            return typeMirror
        }
    }

    /**
     * Maps infra interface types to their respective minecraft type
     */
    protected fun mapToRuntimeType(typeMirror: TypeMirror, semanticVersion: Semver): TypeMirror {
        val targetTypeAnnotation =
            processingEnv.typeUtils.asElement(typeMirror)?.getAnnotation(TargetType::class.java) ?: return typeMirror

        val runtimeTypeName =
            selectValueByVersionConstraint(targetTypeAnnotation.values, targetTypeAnnotation.versions, semanticVersion)

        try {
            return processingEnv.elementUtils.getTypeElement(runtimeTypeName).asType()
        } catch (e: NullPointerException) {
            throw IllegalArgumentException(runtimeTypeName)
        }
    }

    /**
     * Try to map the given type mirror to a platform type. Return the [TypeElement] corresponding to the given [typeMirror]
     * if that fails.
     */
    protected fun tryMapToRuntimeElement(typeMirror: TypeMirror, semanticVersion: Semver): TypeElement {
        return try {
            mapToRuntimeElement(typeMirror, semanticVersion)
        } catch (e: java.lang.IllegalArgumentException) {
            processingEnv.typeUtils.asElement(typeMirror) as TypeElement
        }
    }

    /**
     * Maps infra interface types to their respective minecraft type
     *
     * @throws java.lang.IllegalArgumentException if [typeMirror] has no [TargetType]
     */
    protected fun mapToRuntimeElement(typeMirror: TypeMirror, semanticVersion: Semver): TypeElement {
        val targetTypeAnnotation =
            processingEnv.typeUtils.asElement(typeMirror)?.getAnnotation(TargetType::class.java)
                ?: throw java.lang.IllegalArgumentException("cannot map type $typeMirror to platform element")

        val runtimeTypeName =
            selectValueByVersionConstraint(targetTypeAnnotation.values, targetTypeAnnotation.versions, semanticVersion)

        try {
            return processingEnv.elementUtils.getTypeElement(runtimeTypeName)
        } catch (e: NullPointerException) {
            throw IllegalArgumentException(runtimeTypeName)
        }
    }

    /**
     * Select a value from an array of options by taking the last item that matches the semantic version given.
     */
    protected fun <T> selectValueByVersionConstraint(
        values: Array<out T>,
        versionConstraints: Array<out String>,
        semanticVersion: Semver
    ): T {
        // find the correct injection target by version
        val versionIndex = versionConstraints
            .withIndex()
            .reversed()
            .firstOrNull { (_, version) -> semanticVersion.satisfies(version) }
            ?.index ?: throw IllegalStateException()

        return try {
            values[versionIndex]
        } catch (e: ArrayIndexOutOfBoundsException) {
            throw IllegalArgumentException()
        }
    }

    /**
     * Generate the method body for a proxy method that delegates a call to another platform method. If necessary,
     * the proxy method unwraps surrogates.
     *
     * @param methodElement the [ExecutableElement] of the API interface that is the proxy
     * @param injectionTarget name of the platform method that is being proxied
     * @param semanticVersion the semantic Minecraft version that is injected
     * @param callback if the generated method is an intrinsic proxy
     */
    protected fun generateProxyMethodCode(
        methodElement: ExecutableElement,
        injectionTarget: String,
        semanticVersion: Semver,
        callback: Boolean = false
    ): CodeBlock {
        var codeBlock: CodeBlock =
            generateSurrogateUnpackingCode(methodElement, CodeBlock.of(""))

        // generate return statement
        if (methodElement.returnType !is NoType) {
            codeBlock = if (callback) {
                CodeBlock.join(
                    listOf(codeBlock, CodeBlock.of("info.setReturnValue((\$T)", methodElement.returnType)),
                    ""
                )
            } else {
                CodeBlock.join(listOf(codeBlock, CodeBlock.of("return (\$T)", methodElement.returnType)), "")
            }
        }

        codeBlock = CodeBlock.join(
            listOf(
                codeBlock, CodeBlock.of(
                    "$injectionTarget(${methodElement
                        .parameters
                        .joinToString {
                            "(${mapToRuntimeType(it.asType(), semanticVersion)}) ${it.simpleName}"
                        }})"
                )
            ), ""
        )

        codeBlock = if (methodElement.returnType !is NoType && callback) {
            CodeBlock.join(listOf(codeBlock, CodeBlock.of(");")), "")
        } else {
            CodeBlock.join(listOf(codeBlock, CodeBlock.of(";")), "")
        }

        return codeBlock
    }

    protected fun generateSurrogateUnpackingCode(
        methodElement: ExecutableElement,
        codeBlock: CodeBlock
    ): CodeBlock {
        // generate surrogate unpacking
        val potentialSurrogates = methodElement.parameters
            .filter {
                (it.asType() as? DeclaredType)?.asElement()?.getAnnotation(PlatformSurrogate::class.java) != null
                        || (it.asType() as? DeclaredType)?.asElement()?.getAnnotation(Surrogated::class.java) != null
            }
            .map { param ->
                CodeBlock.of(
                    "if (${param.simpleName} instanceof \$T) { ${param.simpleName} =" +
                            " (${param.asType()}) ((\$T) ${param.simpleName})" +
                            ".${GenerateSurrogatesProcessor.PLATFORM_HANDLE_NAME}; }",
                    ClassName.get(
                        "infra.processors.surrogate",
                        "Surrogate"
                    ),
                    ClassName.get(
                        "infra.processors.surrogate",
                        "Surrogate"
                    )
                )
            }

        return CodeBlock.join(listOf(codeBlock, *potentialSurrogates.toTypedArray()), "\n")
    }

    /**
     * Generate a byte code descriptor from a set of types and an optional return value.
     * @param types parameter type elements in correct order
     * @param returnType optional return type. If null, void is assumed as type
     */
    protected fun generateByteCodeDescriptor(
        vararg types: TypeMirror,
        returnType: TypeMirror? = null
    ): String {
        var descriptor = "("
        types.map { generateByteCodeType(it) }.forEach { descriptor += it }
        descriptor += ")"
        descriptor += generateByteCodeType(returnType)
        return descriptor
    }

    /**
     * Generate the byte-code type-descriptor for the given type as a string. If the given type is null, void type
     * descriptor is returned.
     */
    protected fun generateByteCodeType(type: TypeMirror?): String {
        if (type == null)
            return "V"


        if (type is ArrayType) {
            return "[${generateByteCodeType(type.componentType)};"
        }

        return if (type is PrimitiveType) {
            when (TypeName.get(type).toString()) {
                "byte" -> "B"
                "char" -> "C"
                "double" -> "D"
                "float" -> "F"
                "int" -> "I"
                "long" -> "J"
                "short" -> "S"
                "boolean" -> "Z"
                else -> throw AssertionError("unknown type")
            }
        } else {
            val name = ClassName.get(processingEnv.typeUtils.asElement(type) as TypeElement)
            "L${name.packageName()}.${name.simpleName()};"
        }
    }
}