package infra.processors.surrogate

import com.squareup.javapoet.*
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.RoundEnvironment
import javax.annotation.processing.SupportedAnnotationTypes
import javax.lang.model.SourceVersion
import javax.lang.model.element.*
import javax.lang.model.type.MirroredTypesException
import javax.lang.model.type.TypeKind
import javax.lang.model.type.TypeMirror
import javax.tools.Diagnostic

@SupportedAnnotationTypes("infra.processors.surrogate.PlatformSurrogate")
class GenerateSurrogatesProcessor : AbstractProcessor() {

    companion object {
        /**
         * Name of the variable that holds the platform instance surrogated by the generated class
         */
        public const val PLATFORM_HANDLE_NAME = "handle"
    }

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latestSupported()
    }

    override fun process(annotations: MutableSet<out TypeElement>, roundEnv: RoundEnvironment): Boolean {
        roundEnv.getElementsAnnotatedWith(PlatformSurrogate::class.java)
            .filter { verifyElement(it) }
            .filterIsInstance<TypeElement>()
            .forEach { generateSurrogate(it) }

        return true
    }

    /**
     * Verify the given element is a standard class and nothing else. Print diagnostics if it is something different.
     */
    private fun verifyElement(element: Element): Boolean {
        if (element.kind != ElementKind.INTERFACE) {
            processingEnv.messager
                .printMessage(Diagnostic.Kind.ERROR, "PlatformSurrogate can only annotate class types", element)
            return false
        }

        return true
    }

    private fun generateSurrogate(surrogatedInterface: TypeElement) {
        val surrogateConstructorType = try {
            surrogatedInterface.getAnnotation(PlatformSurrogate::class.java).value
            throw AssertionError("the previous call already must fail")
        } catch (e: MirroredTypesException) {
            e.typeMirrors.map(processingEnv.typeUtils::asElement)[0] as TypeElement
        }

        val surrogateType = TypeSpec.classBuilder("${surrogatedInterface.simpleName}Surrogate")
            .superclass(
                ParameterizedTypeName.get(
                    ClassName.get("infra.processors.surrogate", "Surrogate"),
                    TypeName.get(surrogatedInterface.asType())
                )
            )
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .addSuperinterface(TypeName.get(surrogatedInterface.asType()))

        surrogateConstructorType.enclosedElements
            .filterIsInstance<ExecutableElement>()
            .filter { it.kind == ElementKind.CONSTRUCTOR }
            .forEach { ctor ->
                val ctorBuilder = MethodSpec.constructorBuilder()
                    .addModifiers(Modifier.PUBLIC)

                ctor.parameters.forEach {
                    ctorBuilder.addParameter(
                        ParameterSpec
                            .builder(TypeName.get(it.asType()), it.simpleName.toString())
                            .build()
                    )
                }

                surrogateType.addMethod(ctorBuilder.build())
            }

        generateDelegates(surrogatedInterface.asType(), surrogateType)

        val packageName = surrogatedInterface.qualifiedName
            .substring(0, surrogatedInterface.qualifiedName.lastIndexOf('.'))
        JavaFile.builder(packageName, surrogateType.build()).build().writeTo(processingEnv.filer)
    }

    /**
     * Recursively generate delegates for all methods of the given [typeElement] and all of its supertypes and
     * interfaces. The generated methods are appended to the [surrogateType] builder.
     */
    private fun generateDelegates(typeElement: TypeMirror, surrogateType: TypeSpec.Builder) {
        val type = processingEnv.typeUtils.asElement(typeElement)
        type.enclosedElements
            .filterIsInstance<ExecutableElement>()
            .filter { it.kind == ElementKind.METHOD }
            .forEach { method -> surrogateType.addMethod(generateDelegateMethod(method)) }

        (type as TypeElement).superclass.takeIf { it.kind != TypeKind.NONE }
            ?.apply { generateDelegates(this, surrogateType) }

        type.interfaces.forEach { generateDelegates(it, surrogateType) }
    }

    /**
     * Generate a delegate method that delegates the method call to the platform handle for a given method element.
     */
    private fun generateDelegateMethod(method: ExecutableElement): MethodSpec {
        val methodBuilder = MethodSpec.methodBuilder(method.simpleName.toString())

        // add method parameters
        method.parameters.forEach {
            methodBuilder.addParameter(
                ParameterSpec
                    .builder(TypeName.get(it.asType()), it.simpleName.toString())
                    .build()
            )
        }

        // set return type and visibility
        methodBuilder.returns(TypeName.get(method.returnType))
        methodBuilder.addModifiers(Modifier.PUBLIC)

        // add delegation call
        if (method.returnType.kind == TypeKind.VOID) {

            methodBuilder.addCode(
                "$PLATFORM_HANDLE_NAME.\$N" +
                        "(${(1..method.parameters.count()).joinToString(", ") { "\$N" }});",
                method.simpleName,
                *method.parameters.map(VariableElement::getSimpleName).toTypedArray()
            )
        } else {
            methodBuilder.addCode(
                "return $PLATFORM_HANDLE_NAME.\$N" +
                        "(${(1..method.parameters.count()).joinToString(", ") { "\$N" }});",
                method.simpleName,
                *method.parameters.map(VariableElement::getSimpleName).toTypedArray()
            )
        }

        // add method to type
        return methodBuilder.build()
    }
}