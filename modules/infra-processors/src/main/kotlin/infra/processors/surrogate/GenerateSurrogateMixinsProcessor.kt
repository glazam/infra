package infra.processors.surrogate

import com.squareup.javapoet.*
import com.vdurmont.semver4j.Semver
import infra.processors.AbstractMixinGeneratorProcessor
import javax.annotation.processing.SupportedAnnotationTypes
import javax.annotation.processing.SupportedOptions
import javax.lang.model.element.*
import javax.lang.model.type.MirroredTypesException
import javax.tools.StandardLocation

@SupportedAnnotationTypes("infra.processors.surrogate.GenerateSurrogateMixins")
@SupportedOptions("mc_version")
class GenerateSurrogateMixinsProcessor : AbstractMixinGeneratorProcessor(GenerateSurrogateMixins::class) {

    companion object {
        private const val GEN_MIXIN_PACKAGE_NAME = "infra.surrogates"
    }

    /**
     * Generate surrogate mixins for a dummy annotated with [GenerateSurrogateMixins].
     *
     * @param dummy the dummy object that is annotated with the surrogates to generate mixins for
     */
    override fun generateMixins(dummy: Element, semanticVersion: Semver) {
        val typeMirrors = try {
            dummy.getAnnotation(GenerateSurrogateMixins::class.java).values
            throw AssertionError("the previous call already must fail")
        } catch (e: MirroredTypesException) {
            e.typeMirrors.map(processingEnv.typeUtils::asElement)
        }

        val generatedClasses = mutableListOf<String>()

        typeMirrors.forEach { surrogateTypeMirror ->
            val surrogateInterface = (surrogateTypeMirror as TypeElement).interfaces[0]
            val runtimeTargetType = mapToRuntimeType(surrogateInterface, semanticVersion)
            val mixinClassName = surrogateTypeMirror.simpleName.toString().removePrefix("Infra")

            val mixinClassBuilder = TypeSpec.classBuilder(mixinClassName)
                .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                .addAnnotation(
                    AnnotationSpec.builder(ClassName.get("org.spongepowered.asm.mixin", "Mixin"))
                        .addMember("value", "$surrogateTypeMirror.class")
                        .build()
                )
                .superclass(
                    ParameterizedTypeName.get(
                        ClassName.get("infra.processors.surrogate", "Surrogate"),
                        TypeName.get(surrogateInterface)
                    )
                )

            surrogateTypeMirror.enclosedElements
                .filterIsInstance<ExecutableElement>()
                .filter { it.kind == ElementKind.CONSTRUCTOR }
                .forEachIndexed { index, ctor ->
                    val constructorMixinBuilder = MethodSpec.methodBuilder("onConstructed$index")
                        .addAnnotation(
                            AnnotationSpec
                                .builder(ClassName.get("org.spongepowered.asm.mixin.injection", "Inject"))
                                .addMember(
                                    "method", "\"<init>${
                                    generateByteCodeDescriptor(
                                        *ctor.parameters.map(Element::asType)
                                            .toTypedArray()
                                    )}\""
                                )
                                .addMember("at", "@org.spongepowered.asm.mixin.injection.At(\"RETURN\")")
                                .build()
                        )
                        .addModifiers(Modifier.PUBLIC)

                    // add constructor parameters
                    ctor.parameters.forEach { param ->
                        constructorMixinBuilder.addParameter(ParameterSpec.get(param))
                    }

                    // add callback info parameter
                    constructorMixinBuilder.addParameter(
                        ParameterSpec.builder(
                            TypeName.get(
                                processingEnv.elementUtils.getTypeElement(
                                    "org.spongepowered.asm.mixin.injection.callback.CallbackInfo"
                                ).asType()
                            ),
                            "info"
                        ).build()
                    )

                    // add surrogate initializer
                    constructorMixinBuilder.addCode(
                        "this.${GenerateSurrogatesProcessor.PLATFORM_HANDLE_NAME} = (\$T) new \$T" +
                                "(${(1..ctor.parameters.count()).joinToString(", ") { "(\$T) \$N" }});",
                        TypeName.get(surrogateInterface),
                        TypeName.get(runtimeTargetType),
                        *ctor.parameters.map {
                            listOf(
                                tryMapToRuntimeType(it.asType(), semanticVersion),
                                it.simpleName
                            )
                        }
                            .flatten()
                            .toTypedArray()
                    )

                    mixinClassBuilder.addMethod(constructorMixinBuilder.build())
                }

            JavaFile.builder(GEN_MIXIN_PACKAGE_NAME, mixinClassBuilder.build()).build().writeTo(processingEnv.filer)
            generatedClasses += mixinClassName
        }

        processingEnv.filer.createResource(
            StandardLocation.CLASS_OUTPUT,
            "",
            dummy.getAnnotation(GenerateSurrogateMixins::class.java).mixinMetaFileName
        )
            .openWriter().use { writer ->
                writer.write(
                    """
                    {
                      "required": true,
                      "package": $GEN_MIXIN_PACKAGE_NAME,
                      "refmap": "net.cydhra.refmap.json",
                      "compatibilityLevel": "JAVA_8",
                      "client": [ ${generatedClasses.joinToString(",") { "\"$it\"" }} ]
                    }
                """.trimIndent()
                )
            }
    }
}