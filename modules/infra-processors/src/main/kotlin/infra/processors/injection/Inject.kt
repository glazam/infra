package infra.processors.injection

/**
 * Used within the core module to annotate functions and properties to add meta information about their runtime
 * injection. This meta information is used by [GenerateInjectionsProcessor] to create the respective injection-mixins.
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
annotation class Inject(
    /**
     * The runtime targets as fully qualified symbols. The index of the selected semver expression specifies the
     * target used during injection.
     * @see [versions]
     */
    vararg val values: String,

    /**
     * A set of semver expressions marking which runtime target to use in which version. The number of version
     * expressions must match the number of runtime targets supplied. The versions are matched back-to-front and the
     * first matching expression's index is used for target selection.
     */
    val versions: Array<String> = ["*"],

    /**
     * A set of injection types. If multiple are present, one is selected by looking at the matching semver expression.
     * @see [versions]
     */
    val types: Array<InjectionType> = [InjectionType.FIELD]
)