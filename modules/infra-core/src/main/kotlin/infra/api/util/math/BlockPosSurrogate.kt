package infra.api.util.math

/**
 * Surrogate constructor template for block positions
 */
class BlockPosSurrogate private constructor(x: Int, y: Int, z: Int) {

    /**
     * Secondary constructor from doubles. Invocation to primary constructor is only defined for semantic validity
     */
    private constructor(x: Double, y: Double, z: Double) : this(0, 0, 0)
}