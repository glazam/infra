package infra.api.entity

import infra.processors.injection.TargetType

@TargetType("net.minecraft.entity.Entity")
interface InfraEntity {
    var isSprinting: Boolean
}