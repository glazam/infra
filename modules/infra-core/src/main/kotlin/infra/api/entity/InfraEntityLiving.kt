package infra.api.entity

import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.entity.EntityLivingBase",
    "net.minecraft.entity.LivingEntity",
    versions = ["1.8.9", "^1.14.4"]
)
interface InfraEntityLiving {
}