package infra.api.client.gui

import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.client.gui.GuiButton",
    "net.minecraft.client.gui.widget.ButtonWidget",
    versions = ["1.8.9", "^1.14.4"]
)
interface InfraGuiButton {

//    var enabled: Boolean
//        @Inject("enabled", "active", version = ["1.8.9", "^1.14.4"]) get
//        @Inject("enabled", "active", version = ["1.8.9", "^1.14.4"]) set
}