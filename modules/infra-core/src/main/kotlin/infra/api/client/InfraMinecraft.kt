package infra.api.client

import infra.api.client.gui.InfraGuiScreen
import infra.api.entity.player.InfraPlayerClient
import infra.api.render.InfraRenderGlobal
import infra.api.render.tessellator.InfraTessellator
import infra.api.world.InfraWorldClient
import infra.processors.injection.Inject
import infra.processors.injection.InjectionType.*
import infra.processors.injection.TargetType

@TargetType("net.minecraft.client.Minecraft", "net.minecraft.client.MinecraftClient", versions = ["1.8.9", "^1.14.4"])
interface InfraMinecraft {

    val displayWidth: Int
        @Inject(
            "displayWidth",
            "net.minecraft.client.WindowSettings windowSettings.width",
            versions = ["1.8.9", "^1.14.4"],
            types = [FIELD, DELEGATION]
        ) get

    val displayHeight: Int
        @Inject(
            "displayHeight",
            // omit "net.minecraft.client.WindowSettings " here, so field isn't injected twice
            "windowSettings.height",
            versions = ["1.8.9", "^1.14.4"],
            types = [FIELD, DELEGATION]
        ) get

    val thePlayer: InfraPlayerClient?
        @Inject("thePlayer", "player", versions = ["1.8.9", "^1.14.4"]) get

    val theWorld: InfraWorldClient?
        @Inject("theWorld", "world", versions = ["1.8.9", "^1.14.4"]) get

    val renderGlobal: InfraRenderGlobal
        @Inject("renderGlobal", "worldRenderer", versions = ["1.8.9", "^1.14.4"]) get

    var currentScreen: InfraGuiScreen
        @Inject("currentScreen") get
        @Inject("displayGuiScreen", "openScreen", versions = ["1.8.9", "^1.14.4"], types = [METHOD, METHOD]) set

    val tessellator: InfraTessellator
        @Inject(
            "net.minecraft.client.renderer.Tessellator.getInstance()",
            "net.minecraft.client.render.Tessellator.getInstance()",
            versions = ["1.8.9", "^1.14.4"], types = [DELEGATION, DELEGATION]
        ) get
}