package infra.api.render

import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.client.renderer.RenderGlobal",
    "net.minecraft.client.render.WorldRenderer",
    versions = ["1.8.9", "^1.14.4"]
)
interface InfraRenderGlobal {

}