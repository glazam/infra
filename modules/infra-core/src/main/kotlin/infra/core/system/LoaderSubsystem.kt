package infra.core.system

import infra.core.InfraClient

/**
 * This subsystem invokes a module loader and all module loaders that are registered during the execution of previous
 * ones.
 */
object LoaderSubsystem : SubSystem<ModuleLoader> {

    private val moduleLoaders: MutableList<ModuleLoader> = mutableListOf(InternalModuleLoader)

    /**
     * Call all module loaders while expecting the list of loaders to be modified during this method call
     */
    override fun init() {
        InfraClient.logger.info("invoke module loaders...")
        var index = 0
        while (index < moduleLoaders.size) {
            moduleLoaders[index++].loadModules()
        }

        InfraClient.logger.info("successfully invoked $index module loaders")
    }

    override fun registerModule(module: ModuleLoader) {
        this.moduleLoaders += module
    }

    override fun getModules(): List<ModuleLoader> {
        return moduleLoaders
    }
}