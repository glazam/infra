package infra.core.modulesystem

import infra.core.InfraClient
import infra.core.bus.EventBroker
import infra.core.bus.EventListener

/**
 * An in-game module performing cheats.
 *
 * @param name (unfriendly) name of the module
 * @param tags an array of tags this module is categorized in. Useful for menus
 * @param hide whether to hide this module from general module GUIs.
 * @param description a display description for the client
 */
abstract class Module(
    val name: String,
    vararg val tags: String,
    hide: Boolean = false,
    val description: String = "no description provided"
) : EventListener {

    protected val mc = InfraClient.minecraft

    /**
     * Whether to hide this module from the user. This does not mean, that the user cannot see this module in certain
     * menus, it just means, that GUIs specifically designed to display information about currently active game-affecting
     * modules will not contain this module. (Intended for menu-providing modules, module loaders, etc)
     */
    val hidden: Boolean = hide

    /**
     * A friendly name of the module. Must not contain additional meta information like states or settings, that shall
     * be printed in an array list as well. Use [getDisplayDescriptor] for that
     */
    open val displayName: String
        get() = this.name

    /**
     * Whether the module is currently enabled. If disabled, event listeners are disabled as well. The module may
     * still do stuff if it is disabled, but only for configuration purposes etc, no effects should be visible for
     * the user.
     */
    var isEnabled: Boolean = false
        set(value) {
            if (field != value) {
                field = value

                if (value) {
                    EventBroker.registerListener(this)
                    this.onEnable()
                } else {
                    this.onDisable()
                    EventBroker.unregisterListener(this)
                }
            }
        }

    /**
     * Toggles the module on or off
     */
    fun toggle() {
        this.isEnabled = !this.isEnabled
    }

    /**
     * Initialize the module
     */
    fun init() {
        this.onInitialize()
    }

    /**
     * Called upon module initialization
     */
    protected open fun onInitialize() {

    }

    /**
     * Is called when the module is enabled
     */
    protected open fun onEnable() {

    }

    /**
     * Is called when the module is disabled
     */
    protected open fun onDisable() {

    }

    /**
     * Returns a string that describes the module with a friendly name and optionally additional meta information about
     * the current state of the module. Do not use within menus, use [displayName] instead. This method does not need to
     * return anything but an empty String for modules that set [hidden] to true. If [hidden] is false, it is expected,
     * that this method returns at least [displayName].
     */
    fun getDisplayDescriptor(): String {
        return this.displayName
    }
}