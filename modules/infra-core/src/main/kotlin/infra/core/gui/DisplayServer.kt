package infra.core.gui

import infra.api.client.gui.InfraGuiScreen
import infra.core.gui.controller.DisplayController
import infra.core.gui.theme.Theme
import infra.core.system.SubSystem

/**
 * The subsystem responsible for displaying various gui screens
 */
object DisplayServer : SubSystem<DisplayProvider<*>> {

    /**
     * A list of all available [DisplayProvider]s
     */
    private val providers = mutableListOf<DisplayProvider<in Theme>>()

    /**
     * The currently selected [DisplayProvider]
     */
    private lateinit var activeProvider: DisplayProvider<in Theme>

    /**
     * The chosen [Theme] for the [activeProvider]
     */
    private lateinit var activeTheme: Theme

    override fun init() {

    }

    override fun registerModule(module: DisplayProvider<*>) {
        @Suppress("UNCHECKED_CAST")
        providers += module as DisplayProvider<in Theme>

        if (providers.size == 1) {
            @Suppress("UNCHECKED_CAST")
            activeProvider = module as DisplayProvider<in Theme>
            activeTheme = module.getThemes()[0]
        }
    }

    override fun getModules(): List<DisplayProvider<in Theme>> {
        return providers
    }

    /**
     * Get a list of all themes available to the current display provider
     */
    fun availableThemes(): List<Theme> {
        @Suppress("UNCHECKED_CAST")
        return activeProvider.getThemes() as List<Theme>
    }

    /**
     * Set the theme for the current [DisplayProvider].
     *
     * @param theme the theme name for the provider
     *
     * @throws NoSuchElementException if no theme with this name exists
     */
    fun setTheme(theme: String) {
        activeTheme = activeProvider.getThemes().first { (it as Theme).name == theme } as Theme
    }

    /**
     * Provide a gui screen for the given [DisplayController]
     */
    fun provideScreen(controller: DisplayController): InfraGuiScreen {
        return this.activeProvider.provide(controller, this.activeTheme)
    }
}